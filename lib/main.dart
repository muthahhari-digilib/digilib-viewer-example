import 'dart:convert';
import 'dart:ffi';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_book_component_app/book_page.dart';
import 'package:flutter_book_component_app/video_page.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class MyBookInfo {
  final File bookFile;
  final List<String> bookKeys;

  MyBookInfo(this.bookFile, this.bookKeys);
}

class _MyHomePageState extends State<MyHomePage> {
  static const PRIVATE_KEY =
      '{"exponent":"287299897424633555846893525140322839091955008412859210403078238545552445793736409279477494027451051504665874881079204523545744055296096670998150261064981177775300436488943838538476852509749694595698196707447363011570761035039024412986035729624393131739955786929686949206256644533049981442481381941793780323674534771441658551280486861261223994236277544821933547546143731296861692205510825336100173789007075044690920884816473974175943823684591980572553936744993082877243072266160021465486813221688301506821042157700179586733637086401791379324157347268131012270557868952695737525340451134155872438285062514755406944273","modulo":"16826428398139597273939107200287165241795759952058582727601911098802386630906258315414760076923198894067281002753608424360694752593333590283472541250594880650455643169058009246019801146498182068559671776243054360758992820333648385124098144425731771827382915467391325817810939868420461692400270177229078624730071242353239118686187464575256411942743097338089297200736301058728869317733527398054138887306030513320858195582028885625110734426163952368041544231148351127185677053684963870856190621799711673616255893854168325593113102805363821518996210383491388232596763924046018834028969472568015557917195015168811062999211","p":"143240930493520389029202135961671327555140397167725051398814766391231977023119200608730599765302849940269645002652184631122840302009860464748040632518424036512773629828511195184632745273555790889906296744935558521636422348649310761810271096617851454532374183963628094735516400813684483906443143268962984229881","q":"117469415621401268320668671218925681977124541462180812848101379020010243237161584351577108264448237818560952698932326690530494327177233430743488091519460895386148082384308505176822149886489916817028203106376047164629161483064643351674500441554946830633155551027144492467503879637107292073407687780522694580931"}';
  static final returnDate = DateTime(2020, 12, 4);
  static const FILE_URL = 'https://digilib.ijabi.my.id/get/1';
  static const KEYS_URL = 'https://digilib.ijabi.my.id/keys/1';

  bool isDownloadingBook = false;
  double progress = 0.0;

  Future<MyBookInfo> downloadBook() async {
    final client = new HttpClient();
    final dir = await getTemporaryDirectory();

    final filename = 'file2.zip';
    final files = dir.listSync();
    for (var file in files) {
      if (basename(file.path) != filename) {
        await file.delete(recursive: true);
      }
    }

    final file = new File(join(dir.path, filename));
    if (!await file.exists()) {
      var request = await client.getUrl(Uri.parse(FILE_URL));
      var response = await request.close();
      final f = await file.open(mode: FileMode.write);
      // TODO: cancel download kalau back
      var s = response.listen((data) {
        f.writeFromSync(data, 0, data.length);
        setState(() {
          progress += data.length * 1.0 / response.contentLength;
        });
      }, onDone: () {
        f.close();
      });

      await s.asFuture();
    }

    var resp = await http.get(Uri.parse(KEYS_URL));
    List<dynamic> bookKeys = jsonDecode(resp.body);

    return MyBookInfo(file, bookKeys.map((e) => e.toString()).toList());
  }

  @override
  Widget build(BuildContext context) {
    var body;
    if (isDownloadingBook) {
      body = Center(
        child: LinearProgressIndicator(
          value: progress,
          semanticsLabel: 'Downloading book',
        ),
      );
    } else {
      body = Center(
          child: Column(children: [
        ElevatedButton(
          child: Text('Open Book'),
          onPressed: () async {
            progress = 0;

            setState(() {
              isDownloadingBook = true;
            });
            var book = await downloadBook();
            
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => DigilibBookPage(
                          key: Key('Book'),
                          bookTitle: 'Test Book',
                          bookZipFile: book.bookFile,
                          bookKeys: book.bookKeys,
                          mobilePhonePrivateKey: PRIVATE_KEY,
                          returnDate: returnDate,
                        )));
            setState(() {
              isDownloadingBook = false;
            });
          },
        ),
        ElevatedButton(
          child: Text('Open Video'),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => VideoPage(
                          key: Key('Video'),
                          videoTitle: 'Test Video',
                          videoUrl: 'dG8xqgP4EHY',
                        )));
          },
        ),
      ]));
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: body,
    );
  }
}
