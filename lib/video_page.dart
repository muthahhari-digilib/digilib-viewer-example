
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:webview_flutter/webview_flutter.dart';

class VideoPage extends StatefulWidget {
  final String videoUrl;
  final String videoTitle;

  const VideoPage({Key key, this.videoUrl, this.videoTitle}) : super(key: key);

  @override
  State<StatefulWidget> createState() => VideoPageState();
}

class VideoPageState extends State<VideoPage> {
  bool loading = true;
  Completer t;

  @override
  Widget build(BuildContext context) {
    var embedUrl =
        "https://www.youtube.com/embed/${widget.videoUrl}?autoplay=1";
    var wv = WebView(
        initialUrl: embedUrl,
        javascriptMode: JavascriptMode.unrestricted,
        onPageFinished: (s) {
          print(s);
          setState(() => loading = false);
        });
    return Scaffold(
      appBar: AppBar(title: Text(widget.videoTitle)),
      body: IndexedStack(
        index: loading ? 1 : 0,
        children: [wv, Center(
          child: Center(child: Column(children: [LinearProgressIndicator(), Text('Loading video...')])))],
      )
    );
  }
}
