import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:archive/archive.dart';
import 'package:archive/archive_io.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_book_component_app/digilibcrypto.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';
import 'package:synchronized/synchronized.dart';

class DigilibBookPage extends StatefulWidget {
  DigilibBookPage({Key key, this.bookZipFile, this.bookTitle, this.bookKeys, this.mobilePhonePrivateKey, this.returnDate}) : super(key: key);

  final File bookZipFile;
  final String bookTitle;
  final List<String> bookKeys;
  final String mobilePhonePrivateKey;
  final DateTime returnDate;

  @override
  _DigilibBookPageState createState() => _DigilibBookPageState();

}

class _DigilibBookPageState extends State<DigilibBookPage> {
  bool loading = true;
  bool fullscreen = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: fullscreen ? null : AppBar(title: Text(widget.bookTitle)),
      body: GestureDetector(
        onTap: () {
          setState(() {
            fullscreen = !fullscreen;
          });
        },
        key: Key('mainContent'),
        child: Center(child:FutureBuilder<DigilibBook>(
          future: readBook(),
          builder: (context, snapshot) {
            var width = MediaQuery.of(context).size.width;

            var padding = MediaQuery.of(context).padding;
            var newwidth = width - padding.left - padding.right;

            if (snapshot.hasData) {
              var book = snapshot.data;
              var dim = book.getDimension(0);
              return ListView.builder(
                itemCount: book.pageCount,
                itemBuilder: (context, i) => bookPageBuilder(book, i),
                //itemExtent: dim.height * 1.0 / newwidth * dim.width,
              );
            } else {
              return CircularProgressIndicator();
            }
        }),
      )
    ));
  }

  Widget bookPageBuilder(DigilibBook book, int pageNo) {var width = MediaQuery.of(context).size.width;
    var padding = MediaQuery.of(context).padding;
    var newwidth = width - padding.left - padding.right;
    return FutureBuilder<SvgPicture>(
      future: book.getPage(pageNo),
      builder: (context1, snapshot1) {
        var pict;
        if (snapshot1.hasData) {
          pict = snapshot1.data;
        } else {
          pict = Center(
            child: CircularProgressIndicator()
          );
        }
        final dim = book.getDimension(pageNo);
        return Container(
          child: pict,
          width: newwidth,
          height: dim.height * newwidth / dim.width * 1.0,
        );
      },

    );
  }

  Future<DigilibBook> readBook() async {
    var zipname = path.basenameWithoutExtension(widget.bookZipFile.path);
    var tempDir = await getTemporaryDirectory();
    var outDir = Directory(path.join(tempDir.path, zipname + '_'));
    if (!await outDir.exists()) {
      await outDir.create(recursive:true);
      final d = ZipDecoder().decodeBytes(await widget.bookZipFile.readAsBytes());

      for (final file in d) {
        final data = file.content as List<int>;
        
        final f = File(path.join(outDir.path, file.name));
        await f.create();
        await f.writeAsBytes(data);
      }
    }
    return DigilibBook(outDir, widget.bookKeys, widget.returnDate, widget.mobilePhonePrivateKey);
  }
}

class DigilibBook {
  Directory dir;
  List<String> keys;

  int pageCount;
  bool hasInfo;
  NumberFormat numberFormat;

  DigilibCrypto crypto;
  List<BookPageDimension> dimensions;
  Lock lock;

  DigilibBook (this.dir, this.keys, bookReturnDate, mobilePhonePrivateKey) {
    hasInfo = File(path.join(dir.path, 'info.json')).existsSync();
    crypto = DigilibCrypto(mobilePhonePrivateKey, bookReturnDate);
    numberFormat = NumberFormat('000');
    pageCount = dir.listSync().length;
    if (hasInfo) {
      pageCount --;
    }
    lock = Lock();
  }

  /**
   * pageNo starts from 0
   */
  Future<SvgPicture> getPage(int pageNo) async {
    var fileNo = numberFormat.format(pageNo);
    var f = File(path.join(dir.path, fileNo + '.svg.enc'));
    if (f.existsSync()) {
      final svgCipher = await f.readAsBytes();
      var ret;
      await lock.synchronized(() async {
        ret = await compute(decrypt, DecryptData(crypto, svgCipher, keys[pageNo]));
      });
      return ret;
    }
    f = File(path.join(dir.path, fileNo + '.svg'));
    return SvgPicture.string(await f.readAsString());
  }

  BookPageDimension getDimension(int pageNo) {
    if (dimensions == null) {
      if (!hasInfo) {
        return BookPageDimension(-1, -1);
      }
      Map<String, dynamic> info = jsonDecode(File(path.join(dir.path, 'info.json')).readAsStringSync());
      List<dynamic> dims = info['dimensions'];
      dimensions = dims.map((e) => BookPageDimension(e[0], e[1])).toList();
    }
    return dimensions[pageNo];
  }
}

class BookPageDimension {
  final int width;
  final int height;
  BookPageDimension(this.width, this.height);
}

SvgPicture decrypt(DecryptData d) {
  final svgString = Utf8Decoder().convert(
    d.crypto.decryptSvg(d.svgCipher, d.pageKey)
  ).replaceAll('pt"', '"');
  return SvgPicture.string(svgString);
}

class DecryptData {
  final DigilibCrypto crypto;
  final Uint8List svgCipher;
  final String pageKey;

  DecryptData(this.crypto, this.svgCipher, this.pageKey);
}
