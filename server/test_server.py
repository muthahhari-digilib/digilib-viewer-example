from flask import Flask, abort, send_file, after_this_request
from encrypt import DigilibBookEncryptor
import tempfile
import glob
import json
import os

app = Flask(__name__)

FILE_PATHS = [
    'example/Dimensi Kepribadian Dr. Jalaluddin Rahmat - Rasionalitas, Spiritualitas, dan Keadilan (Prof. Dr. Ali Abbasi) PDF',
    'example/Hymne Miskat(5 Page)'
]

KEYPATH = '../test'

# ini harusnya disimpan di database? atau dikirim kapan gitu
PUBLIC_KEY_MOBILE = '{"exponent":"65537","modulo":"16826428398139597273939107200287165241795759952058582727601911098802386630906258315414760076923198894067281002753608424360694752593333590283472541250594880650455643169058009246019801146498182068559671776243054360758992820333648385124098144425731771827382915467391325817810939868420461692400270177229078624730071242353239118686187464575256411942743097338089297200736301058728869317733527398054138887306030513320858195582028885625110734426163952368041544231148351127185677053684963870856190621799711673616255893854168325593113102805363821518996210383491388232596763924046018834028969472568015557917195015168811062999211"}'

@app.route('/get/<int:file_no>', methods=['GET'])
def get_file(file_no: int):
    if file_no > len(FILE_PATHS):
        abort(404)
    
    files = glob.glob(f'../test/{FILE_PATHS[file_no - 1]}/*.*')
    enc = DigilibBookEncryptor()

    _, outfn = tempfile.mkstemp()

    res = enc.encrypt(
        files,
        '2020-12-04',
        PUBLIC_KEY_MOBILE,
        outfn
    )
    keys = json.dumps(res.keys)

    # kalau di beneran ya disimpan di database, dan dengan user id
    with open(f'{KEYPATH}/key_{file_no}.key', 'w') as f:
        f.write(keys)

    return send_file(outfn, as_attachment=True, attachment_filename='book.zip')


@app.route('/keys/<int:file_no>', methods=['GET'])
def get_keys(file_no: int):
    if file_no > len(FILE_PATHS):
        abort(404)
    
    keyfile = f'{KEYPATH}/key_{file_no}.key'
    if not os.path.exists(keyfile):
        abort(404)
    
    return send_file(keyfile, mimetype='application/json')
