from typing import Dict, List, Optional, Tuple
from dataclasses import dataclass
from zipfile import ZipFile
from functools import partial

from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP, ChaCha20

import json
import random
import string
import base64
import logging
import re
import io


@dataclass
class DigilibEncryptedBook:
    outputfilepath: str
    keys: List[str]

class DigilibBookEncryptor:
    def __init__(self):
        pass

    def encrypt(
        self,
        files: List[str],
        returndate: str,
        mobilephonePublicKey: str,
        outputfilepath: str
    ) -> DigilibEncryptedBook:
        '''
        Mengenkripsi file-file pada list files, menghasilkan file zip dengan isi file 000.svg.enc s/d seluruh halaman.
        Masing-masing file dienkripsi dengan key yang berbeda, dengan metode ChaCha20. Key yang digunakan adalah string
        random + returnDate dikonversi menjadi UTF-8.

        String random sebagai key dan nonce yang dihasilkan dalam proses ini kemudian diubah jadi json:
        {'key' : key_tanpa_returndate, 'nonce': noncebase64} dienkripsi dengan public key mobile phone (RSA 2048),
        dijadikan string base 64. Seluruuh string ini dikembalikan dalam list secara berurutan.

        files: list string path ke file-file buku
        returndate: string tanggal pengembalian (%Y-%m-%d)
        mobilephonePublicKey: public key mobile phone dalam bentuk base64
        '''
        random.seed()
        letters = string.ascii_letters + string.digits
        postfixlen = len(returndate)
        randomkey = lambda: ''.join(random.choice(letters) for _ in range(32-postfixlen))
        keys = [{'key': randomkey(), 'nonce': None} for _ in files]
        dimensions = []
        with ZipFile(outputfilepath, 'w') as z:
            for i, file in enumerate(files):
                key = (keys[i]['key'] + returndate).encode('utf-8')[:32]
                cipher = ChaCha20.new(key=key)
                with open(file, 'rb') as inf:
                    with z.open(f'{i:03}.svg.enc', 'w') as outf:
                        while block := inf.read(1024):
                            outf.write(cipher.encrypt(block))
                keys[i]['nonce'] = base64.b64encode(cipher.nonce).decode('utf-8')

                dimension = [-1, -1]
                if file.lower().endswith('.svg'):
                    try:
                        line = ''
                        with open(file, 'rt') as inf:
                            next(inf)
                            line = next(inf)
                        if not line:
                            raise EOFError(f'Second line not found in file {file}')
                        match = re.search('width="(.*?)".*height="(.*?)"', line)
                        if not match:
                            raise SyntaxError(f'Line 2 in file {file} not contains width.*height: {line}')
                        parse = lambda p: int(p[:-2]) if p.endswith('pt') or p.endswith('px') else int(p)
                        dimension = [parse(match[1]), parse(match[2])]
                    except:
                        logging.exception('Failed to get dimension from SVG')
                else:
                    logging.error(f'File {file} is not end with .svg, get dimension skipped')
                dimensions.append(dimension)

            with io.TextIOWrapper(z.open('info.json', 'w'), 'utf-8') as outf:
                json.dump({
                    'dimensions': dimensions
                }, outf)

        cipher = self.__initRsa(mobilephonePublicKey)
        return DigilibEncryptedBook(outputfilepath, [
            base64.b64encode(cipher.encrypt(json.dumps(key).encode('utf-8'))).decode('utf-8')
            for key in keys
        ])

    @staticmethod
    def __initRsa(mobilephonePublicKey: str):
        key = json.loads(mobilephonePublicKey)
        key = RSA.construct((int(key['modulo']), int(key['exponent'])))
        return PKCS1_OAEP.new(key)


if __name__ == "__main__":
    import glob
    publickey = '{"exponent":"65537","modulo":"19455520507544776392925508703857679159279193212203047012705466220589373196460267708396230713620064981939807999089305548861963582524993349556249539956037615549174439609943510910460944081809073382125499526232694175525827703154732992423867727278153299614938014702283667179240237781246759441251763334360196875862121433438593246941953353106621157423947284706494829279826130340549836405920443237633321300667640604333894781857018217213252273061968843488216962821682874391258470742418772325047026393323440643341462885130804427935841916383204083095737539006732325905419988807546897364795346609915387548578808730870038965045243"}'
    files = glob.glob('../test/example/Dimensi Kepribadian Dr. Jalaluddin Rahmat - Rasionalitas, Spiritualitas, dan Keadilan (Prof. Dr. Ali Abbasi) PDF/*.*')
    print(files)
    enc = DigilibBookEncryptor()

    res = enc.encrypt(
        files,
        '2020-12-04',
        publickey,
        '../test/example/Dimensi Kepribadian Dr. Jalaluddin Rahmat - Rasionalitas, Spiritualitas, dan Keadilan (Prof. Dr. Ali Abbasi) PDF.zip'
    )
    keys = json.dumps(res.keys)
    print(keys)
    print(len(keys))